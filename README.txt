
Spring Boot Docker Integration With Rabbitmq and Mysql
https://www.codeprimers.com/spring-boot-docker-integration-with-rabbitmq-and-mysql/


Spring Boot in a Container
https://spring.io/blog/2018/11/08/spring-boot-in-a-container


tools/
   /mongodb
       /mongo1/
           /db
       /mongo2/
           /db
       /mongo3/
           /db
       /scripts
   /rabbitmq
       /data
       /logs
   /mysql
       /data
