suggested structure


tools/
   /mongodb
       /mongo1/
           /db
       /mongo2/
           /db
       /mongo3/
           /db
       /scripts
   /rabbitmq
       /data
       /logs
   /mysql
       /conf
       /data
