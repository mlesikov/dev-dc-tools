#!/bin/bash

  # Declare a string array with type
declare -a StringArray=("Tarya P2P" "515024131" "טריא פי2פי" "טריא פי2פי בע\"מ"
"Trust account" "Trustee" "Altshuler" "נאמנות" "חשבון נאמנות" "אלטשולר" "נאמן")

# Read the array values with space
for val in "${StringArray[@]}"; do
  echo $val "==================================================================================";
  grep -r -no --exclude-dir={out,build,.idea,dist,node_modules} --exclude=*.sh "$val" .
done
