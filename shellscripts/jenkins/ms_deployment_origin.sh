#!/bin/bash

export IMAGE=$(echo $BRANCH_NAME | sed "s\\/\-\g")
echo "Deploying IMAGE -> ${IMAGE}"

echo connecting to ${SERVER}
export OLDV=$(ssh ${SERVER} 'grep IMAGE_VERSION microservices/.env | cut -d\= -f2')
echo "Previous deployment -> ${OLDV}"

ssh ${SERVER} "set -x ; cd microservices && \
[ x${IMAGE} != x$OLDV ] && docker-compose down || echo 'Deploying same version.' ; \
sed -i '/IMAGE_VERSION=/c\IMAGE_VERSION=${IMAGE}' .env && \
cd git && git fetch && git reset --hard && git checkout ${BRANCH_NAME} && git pull && \
cp -f ../.env . && cd ../ && docker-compose pull"

ssh ${SERVER} '[ -f microservices/git/db-scripts/build.gradle ] && cd microservices/git && export `grep mongodb .env` && \
./gradlew db-scripts:run --args="--mongoURI $MONGODB_URL"'

ssh ${SERVER} 'cd microservices/ && docker-compose up -d'

#cd ../ && sed -i '/external/c\#' docker-compose.yml && \
