#!/bin/bash

export IMAGE=$(echo $BRANCH_NAME | sed "s\\/\-\g")
echo "Deploying IMAGE -> ${IMAGE}"

echo "SERVICE_TO_BUILD  -> ${SERVICE_TO_BUILD}"

echo connecting to ${SERVER}
export OLDV=$(ssh ${SERVER} 'grep IMAGE_VERSION microservices/.env | cut -d\= -f2')
echo "Previous deployment -> ${OLDV}"


if [ ${SERVICE_TO_BUILD} == "ALL" ]; then
    echo "===============================Update ALL services==============================="

    ssh ${SERVER} "set -x ; cd microservices && \
    [ x${IMAGE} != x$OLDV ] && docker-compose down || echo 'Deploying same version.' ; \
    sed -i '/IMAGE_VERSION=/c\IMAGE_VERSION=${IMAGE}' .env && \
    cd git && git fetch && git reset --hard && git checkout ${BRANCH_NAME} && git pull && \
    cp -f ../.env . && cd ../ && docker-compose pull && docker-compose up -d"

else
    echo "===============================Update ${SERVICE_TO_BUILD} from branch ${IMAGE} ==============================="

    ssh ${SERVER} "set -x ; cd microservices && \
    sed -i '/IMAGE_VERSION=/c\IMAGE_VERSION=${IMAGE}' .env && \
    cd git && git fetch && git reset --hard && git checkout ${BRANCH_NAME} && git pull && \
    cp -f ../.env . && cd ../ && docker-compose pull ${SERVICE_TO_BUILD} && \
    docker-compose up -d --no-deps --build ${SERVICE_TO_BUILD}"
fi
