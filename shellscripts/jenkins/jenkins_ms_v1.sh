#!/bin/sh
set -e
echo "DEFINING VARIABLES..."
echo "===================================================================================================="
IMAGE_VERSION=$(echo "$BRANCH_NAME" | sed 's/release\//release-/g')
echo $IMAGE_VERSION
#BRANCH_NAME=$([[ "$IMAGE_VERSION" == "0.1-SNAPSHOT" || "$IMAGE_VERSION" == "1.0.0" ]]  && echo "master" || echo #"${IMAGE_VERSION//-//}")
echo $BRANCH_NAME
echo "===================================================================================================="

echo "STARTING DEPLOY ON LIME ENVIRONMENT..."
echo "===================================================================================================="
echo "LIST CURRENT DOCKER CONTAINERS:"
echo "===================================================================================================="
docker ps

echo "===================================================================================================="
echo "PULL THE LATEST docker-compose.yml FROM GIT REPO:"
echo "===================================================================================================="
cd microservices/git
git status
git reset --hard
git fetch
git checkout $BRANCH_NAME
git pull

echo "===================================================================================================="
echo "REPLACE THE .env CONFIGURATION FILE WITH THOSE FOR LIME ENVIRONMENT:"
echo "===================================================================================================="
rm -rf .env
cp build-config/lime/.env .env
sed -i "/IMAGE_VERSION=/c\IMAGE_VERSION=$IMAGE_VERSION" .env
cat .env

echo "===================================================================================================="
echo "PULL AND START THE LATEST DOCKER CONTAINERS FROM AWS ECR REPOSITORY:"
echo "===================================================================================================="
if [ "$SERVICE_TO_BUILD" = "ALL" ]; then
    echo "Update ALL services"
    docker-compose -f docker-compose.yml pull
    docker-compose -f docker-compose.yml up -d
else
    echo "Update single services"
    docker-compose -f docker-compose.yml pull $SERVICE_TO_BUILD
    docker-compose up -d --no-deps --build $SERVICE_TO_BUILD
fi
docker ps


#IMPORTANT: BUGFIX -> ADD TZDATA package to lender-dashboard to set correct timezone
for i in `docker ps -aqf "name=lender-dashboard|loan-service"`; do docker exec -i $i apk add tzdata; done
